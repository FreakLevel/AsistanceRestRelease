<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marcado extends Model
{
    //
    protected $table = 'marcado';

    public $timestamps = false;

    public $primaryKey = 'id_marcado';
}
