<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registro_Asistencia extends Model
{
    protected $table = 'registro_asistencia';

    public $timestamps = false;

    public $primaryKey = null;

    public $incrementing = false;

    protected $cast = [
        'Hora_marca' => 'hh:mm'
    ];

    protected $fillable = ['RUT', 'id_marcado', 'id_celular', 'estado_ausentismo', 'Fecha_marcado', 'Hora_marca', 'coordX_GPS', 'coordY_GPS'];
}
