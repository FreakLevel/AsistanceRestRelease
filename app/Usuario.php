<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    //
    protected $table = 'usuario';

    public $timestamps = false;

    public $primaryKey = 'RUT';

    public $incrementing = false;
}
