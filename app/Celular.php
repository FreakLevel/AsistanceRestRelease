<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Celular extends Model
{
    //
    protected $table = 'celular';

    public $timestamps = false;

    public $primaryKey = 'id_celular';

    public $incrementing = false;

    protected $fillable = ['Numero_celular', 'Compania', 'IMEI'];
}
