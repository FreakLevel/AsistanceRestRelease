<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Users Section */
Route::group(['prefix' => 'users'], function () {
    Route::get('/{rut}', 'UsuarioController@show')
        ->where(['rut' => '[0-9kK]+']);
    Route::post('/login', 'UsuarioController@login');
    Route::put('/newpassword', 'UsuarioController@newPassword');
    Route::post('/registry', 'RegistroAsistenciaController@registry');
    Route::get('/lastRegistry/{rut}', 'UsuarioController@lastRegister')
        ->where(['rut' => '[0-9kK]+']);
    Route::get('/firstRegistry/{rut}', 'RegistroAsistenciaController@firstRegistry')
        ->where(['rut' => '[0-9kK]+']);
    Route::post('/serchRegistry', 'RegistroAsistenciaController@searchRegistry');
});
